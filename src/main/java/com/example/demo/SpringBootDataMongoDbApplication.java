package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.controller.TutorialController;
import com.example.demo.model.Tutorial;
import com.example.demo.repository.TutorialRepository;

@SpringBootApplication
public class SpringBootDataMongoDbApplication {

	@Autowired
	static TutorialRepository tutorialRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootDataMongoDbApplication.class, args);
	
		tutorialRepository.save(new Tutorial("tut1","desc1",false));

	}

}
